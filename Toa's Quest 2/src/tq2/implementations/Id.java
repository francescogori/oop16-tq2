package tq2.implementations;

/**
 * This enumeration contains the id of the many object used in the Toa's Quest 2 demo.
 * 
 * @author Francesco Gori
 */
public enum Id {
	player, groundGrass, treeSmall, treeBig, mountains, rock, volcano, treeSmall2, sky1, sky2, fireball, light, groundAsh, nuiJaga, rectangleTile, textEntity, attackRectangle, areaRectangle, lava, stoneWall, stoneBackground, sky3, kanohiMask, quickTile;
}
