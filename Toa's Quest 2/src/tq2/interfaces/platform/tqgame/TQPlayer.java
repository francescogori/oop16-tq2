package tq2.interfaces.platform.tqgame;

import tq2.interfaces.platform.*;
/**
 * The Interface TQPlayer was made combining multiple smaller interfaces to obtain a player that implements many behaviors.
 * 
 * @author Francesco Gori
 */

public interface TQPlayer extends Actor, Attack, Crouch, HP, Stamina, Jump, Shoot, Walk, KanohiUser {

}
